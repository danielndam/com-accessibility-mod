<?php

namespace Drupal\bm_slick_accessibility\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\bm_accessibility\Form\AccessibilitySettingsForm;

/**
 * Class slickAccessibilitySettingsForm.
 *
 * The settings form of the bm_slick_accessibility module.
 *
 * @package Drupal\bm_slick_accessibility\Form
 */
class SlickAccessibilitySettingsForm extends AccessibilitySettingsForm {

  const SETTINGS = 'bm_slick_accessibility.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bm_slick_accessibility_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getSettings() {
    return self::SETTINGS;
  }

  /**
   * {@inheritdoc}
   */
  protected function addFormElements(array &$form, $config) {
    $form['slick_a11y'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable slick accessibility fixes (cache flush required).'),
      '#default_value' => $config->get('slick_a11y'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function saveFormElements(FormStateInterface $form_state, $config) {
    $config
      ->set('slick_a11y', $form_state->getValue('slick_a11y'))
      ->save();
  }

}
