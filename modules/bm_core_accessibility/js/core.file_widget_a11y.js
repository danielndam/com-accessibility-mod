/**
 * @file
 * Javascript additionnel dans la gestion des uploads de fichier.
 * Permet de positionner le focus sur le fichier après l'upload.
 */

(($, Drupal, drupalSettings, once) => {
  Drupal.behaviors.bm_core_accessibility_file_widget = {
    attach(context, settings) {
      const { id, action } = settings.bm_core_accessibility.file_widget;
      const selector = `#${id}`;
      once('bm_core_accessibility_file_widget', selector, context).forEach(
        (element) => {
          const ahref = element.querySelector('a');
          if (ahref) {
            const deleteButton = context.querySelector('input[type=submit]');
            deleteButton.setAttribute(
              'aria-label',
              `${deleteButton.value} ${ahref.innerHTML}`,
            );
          }
          setTimeout(() => {
            if (action === 'upload') {
              const $fileLink = $('span.file a', $(element));
              $fileLink.focus();
            } else if (action === 'remove') {
              const $input = $('input[type=file]', $(element));
              $input.focus();
            }
          }, 0);
        },
      );
    },
  };
})(jQuery, Drupal, drupalSettings, once);
