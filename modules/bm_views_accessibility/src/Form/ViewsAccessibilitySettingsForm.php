<?php

namespace Drupal\bm_views_accessibility\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\bm_accessibility\Form\AccessibilitySettingsForm;

/**
 * Class ViewsAccessibilitySettingsForm.
 *
 * The settings form of the bm_views_accessibility module.
 *
 * @package Drupal\bm_views_accessibility\Form
 */
class ViewsAccessibilitySettingsForm extends AccessibilitySettingsForm {

  const SETTINGS = 'bm_views_accessibility.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bm_views_accessibility_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getSettings() {
    return self::SETTINGS;
  }

  /**
   * {@inheritdoc}
   */
  protected function addFormElements(array &$form, $config) {
    $form['views_title_pagination'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add pagination number in HTML > HEAD > TITLE'),
      '#default_value' => $config->get('views_title_pagination'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function saveFormElements(FormStateInterface $form_state, $config) {
    $config
      ->set('views_title_pagination', $form_state->getValue('views_title_pagination'))
      ->save();
  }

}
