/**
 * @file
 * JavaScript's behaviors for select2 to add aria-label.
 */

(($, Drupal, once, drupalSettings) => {
  // @see https://github.com/ractoon/jQuery-Text-Counter#options

  // code from counter js
  Drupal.webform = Drupal.webform || {};
  Drupal.webform.counter = Drupal.webform.counter || {};
  Drupal.webform.counter.options = Drupal.webform.counter.options || {};

  // get counter start limit set in para
  const counterAriaThreshold =
    drupalSettings.webformAccessibility.counter_aria_threshold;

  // code from counter js
  function wordCount(text) {
    return text.trim().replace(/\s+/gi, ' ').split(' ').length;
  }

  // set the text base on type and option
  function setText(option, type, wordText, charText) {
    return (
      option || (type === 'word' ? Drupal.t(wordText) : Drupal.t(charText))
    );
  }

  // get the remaining word or character of a input
  function getRemaining(options, event) {
    return (
      options.max -
      (options.type === 'word'
        ? wordCount(event.target.value)
        : event.target.value.length)
    );
  }

  // update aria live
  function updateAriaLive(options, event, textCountMsg) {
    // Calculate the remaining characters based on the option type (word or character count)
    const remaining = getRemaining(options, event);

    if (remaining <= counterAriaThreshold) {
      // Set "aria-live" to "polite" if remaining characters are less than or equal to 20
      textCountMsg.setAttribute('aria-live', 'polite');
    } else {
      // Set "aria-live" to "off" if remaining characters are above 20
      textCountMsg.setAttribute('aria-live', 'off');
    }
  }

  // set the aria-live according to character or word counter limits
  function initializeCounterListeners(el, options) {
    // get the div of the texte to display the remaining characters or word
    const textCountMsg = el.closest('div').querySelector('.text-count-message');
    // Set aria-live to off by default (Temesis).
    textCountMsg.setAttribute('aria-live', 'off');
    // put the aria-live off when the element loose the focus
    el.addEventListener('focus', (event) => {
      updateAriaLive(options, event, textCountMsg);
    });
    // Add an event listener for "input" events on the found element
    el.addEventListener('input', (event) => {
      updateAriaLive(options, event, textCountMsg);
    });
    // set the aria-live to off when element loose focus
    el.addEventListener('blur', () => {
      textCountMsg.setAttribute('aria-live', 'off');
    });
  }

  Drupal.behaviors.CustomWebformCounter = {
    attach(context) {
      // code from counter js
      $(once('webform-counter', '.js-webform-counter', context)).each(
        function initCounter() {
          let options = {
            type: $(this).data('counter-type'),
            max: $(this).data('counter-maximum'),
            min: $(this).data('counter-minimum') || 0,
            counterText: $(this).data('counter-minimum-message'),
            countDownText: $(this).data('counter-maximum-message'),
            inputErrorClass: 'webform-counter-warning',
            counterErrorClass: 'webform-counter-warning',
            countSpaces: true,
            twoCharCarriageReturn: true,
            stopInputAtMaximum: false,
            // Don't display min/max message since server-side validation will
            // display these messages.
            minimumErrorText: '',
            maximumErrorText: '',
            // Modification added
            init(el) {
              initializeCounterListeners(el, options);
            },
            // end of modification
          };

          options.countDown = !!options.max;
          options.counterText = setText(
            options.counterText,
            options.type,
            '%d word(s) entered',
            '%d character(s) entered',
          );
          options.countDownText = setText(
            options.countDownText,
            options.type,
            '%d word(s) remaining',
            '%d character(s) remaining',
          );

          options = $.extend(options, Drupal.webform.counter.options);

          $(this).textcounter(options);
        },
      );
    },
  };
})(jQuery, Drupal, once, drupalSettings);
