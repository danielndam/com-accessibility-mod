<?php

namespace Drupal\bm_select2_accessibility\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\bm_accessibility\Form\AccessibilitySettingsForm;

/**
 * Class Select2AccessibilitySettingsForm.
 *
 * The settings form of the bm_select2_accessibility module.
 *
 * @package Drupal\bm_select2_accessibility\Form
 */
class Select2AccessibilitySettingsForm extends AccessibilitySettingsForm {

  const SETTINGS = 'bm_select2_accessibility.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bm_select2_accessibility_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getSettings() {
    return self::SETTINGS;
  }

  /**
   * {@inheritdoc}
   */
  protected function addFormElements(array &$form, $config) {

    $form['aria_label_select2_multiple'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add name element in aria-label button when select2 is activated on multiple select'),
      '#default_value' => $config->get('aria_label_select2_multiple'),
    ];

  }

  /**
   * {@inheritdoc}
   */
  protected function saveFormElements(FormStateInterface $form_state, $config) {
    $config
      ->set('aria_label_select2_multiple', $form_state->getValue('aria_label_select2_multiple'))
      ->save();
  }

}
