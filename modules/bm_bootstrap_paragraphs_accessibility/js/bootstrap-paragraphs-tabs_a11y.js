/**
 * @file
 * JavaScript behaviors for accessibility adjustements, Bootstrap Paragraphs module.
 * For tabs paragraphs especially.
 */

(($, Drupal, once) => {
  function init(i, tab) {
    const $tab = $(tab);
    // Add event to tab.
    $tab.on('click keyup', () => {
      // Get the tabpanel id referenced by the clicked tab.
      const tabPanelId = $tab.attr('href');
      // Get the tablist id which the tab belongs.
      const tabListId = $(tabPanelId)
        .parents('div.paragraph--type--bp-tabs')
        .attr('id');

      /* Browse tabpanels of the tablist in order to add or remove tabindex attribut. */
      const $tabPanels = document.querySelectorAll(`#${tabListId} .tab-pane`);
      $tabPanels.forEach((tabPanel) => {
        const $tabPanel = $(tabPanel);
        if (`#${$tabPanel.attr('id')}` === tabPanelId) {
          // Current tabpanel is referenced by the clicked tab, we add tabindex attribute.
          $(tabPanelId).attr('tabindex', 0);
        } else if ($tabPanel.attr('tabindex')) {
          // We remove tabindex attribute to the old active tabpanel.
          $tabPanel.removeAttr('tabindex');
        }
      });
    });
  }

  Drupal.behaviors.paragraphTabs = {
    attach(context) {
      /* A11y, add tabindex attribute to active tabpanels of tabs paragraph.  */
      const $activeTabPanels = document.querySelectorAll(
        '.paragraph--type--bp-tabs .tab-pane.active',
      );
      $activeTabPanels.forEach((activeTabPanel) => {
        const $activeTabPanel = $(activeTabPanel);
        $activeTabPanel.attr('tabindex', 0);
      });

      // A11y, in order to add tabindex attribute to the active tabpanel of tablist paragraphe on tab click.
      once(
        'paragraph-tabs',
        '.paragraph--type--bp-tabs .nav-tabs button.nav-link',
        context,
      ).forEach((value) => {
        $(value).each(init);
      });
    },
  };
})(jQuery, Drupal, once);
