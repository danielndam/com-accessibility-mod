<?php

namespace Drupal\bm_accessibility\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base class AccessibilitySettingsForm.
 *
 * The abstract settings form of the bm_accessibility submodules.
 *
 * @package Drupal\bm_accessibility\Form
 */
abstract class AccessibilitySettingsForm extends ConfigFormBase {

  /**
   * Gets settings form identifier.
   *
   * @return string
   *   The unique string identifying the settings form.
   */
  abstract protected function getSettings();

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [$this->getSettings()];
  }

  /**
   * Add elements to a form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   *   An editable configuration object if the given name is listed in the
   *   getEditableConfigNames() method or an immutable configuration object if
   *   not.
   */
  abstract protected function addFormElements(array &$form, $config);

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this->getSettings());

    $this->addFormElements($form, $config);

    return parent::buildForm($form, $form_state);
  }

  /**
   * Save form elements.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   *   An editable configuration object if the given name is listed in the
   *   getEditableConfigNames() method or an immutable configuration object if
   *   not.
   */
  abstract protected function saveFormElements(FormStateInterface $form_state, $config);

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config($this->getSettings());

    $this->saveFormElements($form_state, $config);

    parent::submitForm($form, $form_state);
  }

}
