# com-accessibility-mod

Ensemble de modules destinés à améliorer l'accessibilité de différents
composants Drupal.

## Modules

### bm_webform_accessibility

Module permettant d'améliorer l'accessibilité du module Webform.

### bm_views_accessibility

Module permettant d'améliorer l'accessibilité du module Views.

Le module injecte le numéro de la page courante de la vue dans le titre `H1`
de la page via un `SPAN` `visually-hidden`.

Le module propose aussi un nouveau token `current-page:title-with-page-number`
à renseigner le cas échéant dans la configuration du module `metatag` afin
d'ajouter le numéro de page dans la balise `title` du `head` de la page HTML.
